package ru.ovechkin.tm.exeption.user;

public class AdminBlockingException extends RuntimeException{

    public AdminBlockingException() {
        super("Error! You can't lock an admin...");
    }

}
