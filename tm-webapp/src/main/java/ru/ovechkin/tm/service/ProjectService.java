package ru.ovechkin.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.repository.ProjectRepository;

import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @Transactional
    public void save(@Nullable final Project project) {
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String projectId) {
        projectRepository.deleteById(projectId);
    }

    @Override
    public Project findById(@Nullable final String projectId) {
        return projectRepository.findById(projectId).get();
    }

    @Override
    @Transactional
    public void updateById(@Nullable final String id, @Nullable final Project project) {
        final Project projectToUpdate = projectRepository.findById(id).get();
        projectToUpdate.setName(project.getName());
        projectToUpdate.setDescription(project.getDescription());
        projectRepository.save(projectToUpdate);
    }

}