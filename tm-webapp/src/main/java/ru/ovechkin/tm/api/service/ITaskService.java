package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll(String projectId);

    void save(@Nullable Task task);

    void removeById(@Nullable String taskId);

    @NotNull Task findById(@Nullable String taskId);

    @Transactional
    void updateById(
            @Nullable String taskId,
            @Nullable String projectId,
            @Nullable Task task
    );

}