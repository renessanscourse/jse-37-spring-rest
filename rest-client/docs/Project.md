
# Project

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**tasks** | [**List&lt;Task&gt;**](Task.md) |  |  [optional]
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**startDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**finishDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**creationTime** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]



